# OpenML dataset: Worldwide-Crop-Production

https://www.openml.org/d/43760

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Crop production depends on the availability of arable land and is affected in particular by yields, macroeconomic uncertainty, as well as consumption patterns; it also has a great incidence on agricultural commodities' prices. The importance of crop production is related to harvested areas, returns per hectare (yields) and quantities produced.
Crop yields are the harvested production per unit of harvested area for crop products. In most of the cases yield data are not recorded, but are obtained by dividing the production data by the data on area harvested. The actual yield that is captured on farm depends on several factors such as the crop's genetic potential, the amount of sunlight, water and nutrients absorbed by the crop, the presence of weeds and pests. This indicator is presented for wheat, maize, rice and soybean. Crop production is measured in tonnes per hectare, in thousand hectares and thousand tonnes.
Content
The csv file has 5 columns:
LOCATION = the country code name
SUBJECT = The type of crop(rice,soybean,etc)
TIME = the year the data was recorded
MEASURE = the measuring metric used
VALUE = The value, according to the measuring metric specified
Acknowledgements
https://data.oecd.org/agroutput/crop-production.htm

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43760) of an [OpenML dataset](https://www.openml.org/d/43760). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43760/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43760/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43760/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

